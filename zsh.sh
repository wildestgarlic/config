#!/bin/bash

echo '\n\n>>> Setup ZSH\n\n'

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions || true
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting || true
touch ~/.zshrc && curl -fsSL -o ~/.zshrc https://gitlab.com/wildestgarlic/config/-/raw/main/zshrc?ref_type=heads&inline=false
chsh -s $(which zsh)
source ~/.zshrc


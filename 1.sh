#!/bin/bash

echo '\n\n>>> Updating and upgrading\n\n'
apt update && apt upgrade -y && apt autoremove -y

echo '\n\n>>> Installing packages\n\n'
apt install -y ca-certificates curl git zsh mtr bat neovim htop make ca-certificates

bash <(curl -s https://gitlab.com/wildestgarlic/config/-/raw/main/docker.sh?ref_type=heads&inline=false)

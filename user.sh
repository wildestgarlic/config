#!/bin/bash

echo '\n\n>>> Creating a user\n\n'
read -p "Enter username: " username
sudo useradd -m -s /bin/bash -G sudo,docker $username && passwd $username
sudo sh -c echo '$username ALL=NOPASSWD: ALL' >> /etc/sudoers
sudo sh -c echo '$username ALL=NOPASSWD: ALL' >> /etc/sudoers
sudo -su $username
